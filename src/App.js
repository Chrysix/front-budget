
import './App.css';
import { Home } from "./pages/Home";
import { Nav } from "./components/Nav";
import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
    return (
        <Router>
            <div>
                {/* <Nav /> */}
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                </Switch>
            </div>
        </Router >
    );
}

export default App;
