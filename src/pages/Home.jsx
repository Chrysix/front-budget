
import { useState, useEffect } from "react"
import { DisplayOP } from "../components/DisplayOP";
import { FormOP } from "../components/FormOP";
import { Nav } from "../components/Nav";
import * as React from 'react';
import { TableOP } from "../components/TableOP";
import { SearchMonth } from "../components/SearchMonth";
import { OPService } from "../services/OPService";



export function Home() {

    const [operation, setOP] = useState([])


    async function addOP(ope) {
        const response = await OPService.add(ope);
        setOP([
            ...operation,
            response.data
        ]);
    }

    async function deleteOP(id) {
        await OPService.delete(id)
        setOP(
            operation.filter(item => item.id !== id)
        );
    }

    async function showOP() {
        const response = await OPService.showAll();
        setOP(response.data);
    }

    async function showMonth(month) {
        const response = await OPService.showPerMonth(month)
        setOP(response.data)
    }

    function totalPrice() {
        let total = 0
        for (const item of operation) {
            total += Number (item.price)
        }
        return total
    }

    useEffect(() => {
        showOP();//affiche showOP une seul fois sur la page sans rechargement 
    }, []); //réactualise opération pour voir si il à changer


    return (
        <>
            <div>
                <nav className="nav">
                    <h1>Opérations</h1>
                    <SearchMonth reset={showOP} selectMonth={showMonth} />
                </nav>

                <h3 className="card" >Total Price : {totalPrice()} €</h3>

                <div className="responsiveBlock col-12 d-flex justify-content-around container">

                    <FormOP onFormSubmit={addOP} />
                    <TableOP op={operation} onDelete={deleteOP} />
                </div>


                {/* <Nav />FormNavSubmit={findId, findCat, findDate}*/}
                {/* {operation.map(item => <DisplayOP key={item.id} onDelete={deleteOP} op={item} />)} */}
            </div>

        </>
    )

};