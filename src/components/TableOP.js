import React from 'react';
import { makeStyles, createTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Box, Container, Grid } from '@material-ui/core';

const useStyles = makeStyles({
    table: {
        minWidth: 50,
        maxWidth: 900

    },
    colmunName: {
        backgroundColor: blue[500],

    },
    alignItemsAndJustifyContent: {
        
        display: 'flex',
        
        justifyContent: 'flex-end',
    },
});


export function TableOP({ op, onDelete }) {

    const classes = useStyles();

    return (
        // <Container maxWidth="ms">

        <Grid xs={6}>
            <Box className={classes.alignItemsAndJustifyContent}>
                <TableContainer className="tableau" component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead className={classes.colmunName}>
                            <TableRow >
                                <TableCell>Label</TableCell>
                                <TableCell>Category</TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell>Price</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {op.map(item => (
                                <TableRow>
                                    <TableCell>{item.label}</TableCell>
                                    <TableCell>{item.category}</TableCell>
                                    <TableCell >{Intl.DateTimeFormat('fr-FR').format(new Date(item.date))}</TableCell>
                                    <TableCell >{Number.parseFloat(item.price).toFixed(2)}€</TableCell>
                                    <button onClick={() => onDelete(item.id)} className="cross">X</button>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
        </Grid>
    );
}