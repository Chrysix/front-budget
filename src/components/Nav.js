import { Link } from "react-router-dom";
import { useState } from "react";
import { SearchMonth } from "./SearchMonth";
import { OPService } from "../services/OPService";



const intialState = {
    id: '',
    category: '',
    date: ''
}

export function Nav({ FormNavSubmit }) {

    const [form, setForm] = useState(intialState);

    const handleChange = event => {

        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    const handleSubmit = event => {

        event.preventDefault()
        FormNavSubmit(form)

    }
    
        return (
            <nav className="nav">
                <h1>Opérations</h1>
                {/* <form onSubmit={handleSubmit}>
                    <label>ID : </label>
                    <input type="text" name="label" onChange={handleChange} value={form.id} />
                    <input type="submit" />

                    <p>or</p>

                    <label>Category : </label>
                    <input className="ms-3" type="text" name="label" onChange={handleChange} value={form.category} />
                    <input type="submit" />

                    <p>or</p>

                    <label>Date : </label>
                    <input className="ms-3" type="date" name="label" onChange={handleChange} value={form.date} />
                    <input className="mb-3" type="submit" />
                </form> */}

            </nav >
        )
    }
