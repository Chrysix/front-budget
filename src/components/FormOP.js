import { useState } from "react";

const intialState = {
    label: '',
    price: '',
    category: '',
    date: ''
}

export function FormOP({ onFormSubmit }) {

    const [form, setForm] = useState(intialState);

    const handleChange = event => {

        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    const handleSubmit = event => {

        event.preventDefault()
        onFormSubmit(form)

    }
    
    return (

        <div className='formBox container'>
            <div className='row'>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label className='formGroupExampleInput form-label'>Nom : </label>
                        <input className='form-control' type="text" name="label" required onChange={handleChange} value={form.label} />
                    </div>

                    <div>
                        <label className='formGroupExampleInput form-label'>Prix : </label>
                        <input className='form-control' type="number" name="price" required onChange={handleChange} value={form.price} />
                    </div>

                    <div>
                        <label className='formGroupExampleInput form-label'>Category : </label>
                        <input className='formGroupExampleInput form-label'
                            className='form-control' className='form-control' type="text" name="category" required onChange={handleChange} value={form.category} />
                    </div>

                    <div>
                        <br />
                        <label className=''
                            className=''>Date : </label>
                        <input className=''
                            className='' type="date" name="date" required onChange={handleChange} value={form.date} />
                    </div>

                    <div className='justify-content-center d-flex'>
                        <button className='buttonSubmit'>Submit</button>
                    </div>

                </form>
            </div>
        </div>
    )
}