export function SearchMonth({ reset, selectMonth }) {

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getMonthsForLocale(locale) {
        var format = new Intl.DateTimeFormat(locale, { month: 'long' })
        var months = []
        for (var month = 0; month < 12; month++) {
            var testDate = new Date(Date.UTC(2000, month, 1, 0, 0, 0));
            months.push(capitalizeFirstLetter(format.format(testDate)))
        }
        return months;
    }

    const months = getMonthsForLocale('fr-FR')


    function handleReset() {
        reset()
    }

    function handleSelect(month) {
        selectMonth(month)
    }

    return (
        <div>
            <div>
                <p class="btn btn-light" onClick={handleReset}>Retour →</p>
            </div>
            <div class="dropdown">
                <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    Rechercher par mois
                </button>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    {months.map((month, index) => <p key={index} className="dropdown-item" onClick={() => handleSelect(index + 1)}>{month}</p>)}
                </div>



            </div>
        </div>
    )
}