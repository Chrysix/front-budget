import { react } from "react";


export function DisplayOP({ op, onDelete }) {


    return (
        <>
        
            <div className="block">
                {<p className="card" key={op.id}> {op.label} {Number.parseFloat(op.price).toFixed(2)}€ {op.category} {op.date}
                    <button onClick={() => onDelete(op.id)} className="cross">X</button>
                </p>}
            </div>
        </>
    )
};




// {new Intl.DateTimeFormat('fr-FR').format(new Date(op.date))}