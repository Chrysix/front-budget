import axios from "axios";


export class OPService {

    static async showAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operation');
    }

    static async showPerMonth(month) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operation/date/'+ month);
    }
    
    static async add(ope) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/operation', ope);
    }

    static async delete(id) {
        return axios.delete(process.env.REACT_APP_SERVER_URL+'/api/operation/'+ id);
    }
}